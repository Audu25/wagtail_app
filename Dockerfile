FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get upgrade
RUN apt-get install libjpeg-dev dos2unix gettext -q -y
RUN mkdir -p /app
WORKDIR /app
ADD . .
RUN pip install -r requirements.txt
RUN dos2unix docker-entrypoint.sh
RUN chmod +x docker-entrypoint.sh
RUN chown -R www-data:www-data /app
USER www-data:www-data

