MIDDLEWARE = [
    ...
    'django.middleware.cache.UpdateCacheMiddleware',             <----  Before CommonMiddleware
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',          <----  After CommonMiddleware
    ...
    'wagtail.core.middleware.SiteMiddleware',
    'wagtail.contrib.redirects.middleware.RedirectMiddleware',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',      <----  We will use postgres
        'NAME': 'mysite',                                        <----  Set a db name
        'USER': 'mysite',                                        <----  Set a db username
        'PASSWORD': 'mysite',                                    <----  Set a db password
        'HOST': 'mysite_db',                                     <----  This will be the postgres docker-compose service
    }
}

WAGTAILSEARCH_BACKENDS = {
    'default': {
        'BACKEND': 'wagtail.search.backends.elasticsearch6',     <----  We will use elasticsearch as backend
        'URLS': ['http://mysite_elasticsearch:9200'],            <----  This is the elasticsearch docker-compose service
        'INDEX': 'wagtail',                                      <----  Elasticsearch will index it with wagtail
        'TIMEOUT': 5,                                            <----  I don't know if you need more settings - i am not an expert in elasticsearch
        'OPTIONS': {},
        'INDEX_SETTINGS': {},
    }
}

CACHES = {
'default': {
    'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',  <----  Using the memcached backend from django
    'LOCATION': 'mysite_cache:11211',                                  <----  This is the memcached docker-compose service
    'TIMEOUT': 600
}

STATIC_ROOT = '/data/static'   <----  We will store the static files in an external docker volume which will be shared with nginx
MEDIA_ROOT = '/data/media'     <----  We will store the media files in an external docker volume which will be shared with nginx

